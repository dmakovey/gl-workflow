#!/bin/sh

# Script is designed to produce "short" version of pipeline JSON for easier digestion
# requires `gl-flow.py pipeline ...` output

exec jq '.[] | {'\
'    id: .id, '\
'    ref: .ref, '\
'    commit: .sha, '\
'    web_url: .web_url, '\
'    jobs: [ '\
'        .jobs[] | {'\
'            name: .name, '\
'            status: .status, '\
'            user: .user.username, '\
'            started: .started_at, '\
'            duration: .duration, '\
'            in_queue: .queued_duration, '\
'            runner_id: .runner.id, '\
'        } '\
'    ] '\
'}' $@