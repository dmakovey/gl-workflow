#!/bin/sh

TRIAGE_ISSUE=${1}

python gl-flow.py get-issue --issue-url "${TRIAGE_ISSUE}" | jq -r '[.discussions[] | .[] | select(.system | not) .body]'