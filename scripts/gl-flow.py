#!/usr/bin/env python

import gitlab
import configparser
import os
from urllib.parse import urlparse, quote_plus as url_quote_plus
import yaml
import argparse
import json
import sys
import logging
import re
from string import Template


class IssueVisisted(Exception):
    issue = None
    visited_list = None

    def __init__(self, issue, visited_list):
        self.issue = issue
        self.visited_list = visited_list


TYPE_MAP = {
    'Issue': 'issue',
    'MergeRequest': 'mr',
    'Commit': 'git_ref'
}


def repository2dict(repo):
    field_list = ['id', 'name', 'path', 'project_id', 'location', 'created_at', 'cleanup_policy_started_at']
    r = {}
    for f in field_list:
        r[f] = getattr(repo, f)
    return r    


class Automator(object):
    gl = None
    dry_run = False

    issue_url_cache = None
    issue_ref_cache = None
    issue_id_cache = None
    issue_raw_cache = None
    project_id_cache = None
    mr_url_cache = None
    mr_ref_cache = None
    mr_id_cache = None
    mr_raw_cache = None
    epic_url_cache = None
    epic_ref_cache = None
    epic_id_cache = None

    def __init__(self, gl_url, gl_token, dry_run=False):
        self.gl = gitlab.Gitlab(gl_url, private_token=gl_token)
        self.gl.auth()
        self.dry_run = dry_run

    def cache(self, *object_types):
        for ot in object_types:
            if ot == 'issue_url':
                self.issue_url_cache = {}
            elif ot == 'issue_ref':
                self.issue_ref_cache = {}
            elif ot == 'issue_id':
                self.issue_id_cache = {}
            elif ot == 'issue_raw':
                self.issue_raw_cache = {}
            elif ot == 'project_id':
                self.project_id_cache = {}
            elif ot == 'mr_url':
                self.mr_url_cache = {}
            elif ot == 'mr_ref':
                self.mr_ref_cache = {}
            elif ot == 'mr_id':
                self.mr_id_cache = {}
            elif ot == 'mr_raw':
                self.mr_raw_cache = {}
            elif ot == 'epic_url':
                self.epic_url_cache = {}
            elif ot == 'epic_ref':
                self.epic_ref_cache = {}
            elif ot == 'epic_id':
                self.epic_id_cache = {}

    def uncache(self, *object_types):
        for ot in object_types:
            if ot == 'issue_url':
                self.issue_url_cache = None
            elif ot == 'issue_ref':
                self.issue_ref_cache = None
            elif ot == 'issue_id':
                self.issue_id_cache = None
            elif ot == 'issue_raw':
                self.issue_raw_cache = None
            elif ot == 'project_id':
                self.project_id_cache = None
            elif ot == 'mr_url':
                self.mr_url_cache = None
            elif ot == 'mr_ref':
                self.mr_ref_cache = None
            elif ot == 'mr_id':
                self.mr_id_cache = None
            elif ot == 'mr_raw':
                self.mr_raw_cache = None

    def get_project_by_id(self, project_id):
        if self.project_id_cache is not None:
            if self.project_id_cache:
                if project_id in self.project_id_cache:
                    logging.debug("Found project {} in cache".format(project_id))
                    return self.project_id_cache[project_id]
        project = self.gl.projects.get(project_id)
        if self.project_id_cache is not None:
            self.project_id_cache[project_id] = project
            logging.debug("Added project {} to cache".format(project_id))
        return project

    def get_project_by_ref(self, project_ref):
        # API allows for either ID or Ref to be used
        # to retrieve proejct object we'll just
        # benefit from everuthing already done in 
        # `get_project_by_id()`
        return self.get_project_by_id(project_ref)

    def parse_uri(self, uri, extended=False):
        """parse URI to determine whether it's Issue or MR
        return corresponding object"""
        issue_re = re.compile(r'^.*#\d+$')
        mr_re = re.compile(r'^.*!\d+$')
        epic_re = re.compile(r'^.*&\d+$')

        issue_url_re = re.compile(r'\-\/issues\/\d+$')
        mr_url_re = re.compile(r'\-/merge_requests/\d+$')
        epic_url_re = re.compile(r'\-/epics/\d+$')

        uri_re = re.compile(r'(?P<proto>\w+(?=://))?(?:://(?P<domain>[\w\.\d]+(?=/)))?(?=:(?P<port>\d+))?/(?P<path>[\w\/\.\d\-_]+)(?P<suffix>[^?]+)?(?:\?(?P<query>.*))?$')
        m = uri_re.match(uri)
        if not m:
            #XXX ugly hack: add '/' to the GitLab refs
            m = uri_re.match('/'+uri)
        uri_decomposed = m.groupdict()
        is_url = uri_decomposed['proto'] or uri_decomposed['domain']
        if is_url:
            if issue_url_re.search(uri):
                issue = self.get_issue_by_url(uri)
                return self.get_issue_raw(issue) if extended else issue
            elif mr_url_re.search(uri):
                mr = self.get_mr_by_url(uri)
                return self.get_mr_raw(mr) if extended else mr
            elif epic_url_re.search(uri):
                raise "Don't know what to do with Epics"
            else:
                raise "Unknown URI"
        else:
            if issue_re.search(uri):
                issue = self.get_issue_by_ref(uri)
                return self.get_issue_raw(issue) if extended else issue
            elif mr_re.search(uri):
                mr = self.get_mr_by_ref(uri)
                return self.get_mr_raw(mr) if extended else mr
            elif epic_re.search(uri):
                raise "Don't know what to do with Epics"
            else:
                raise "Unknown Ref"

    def get_user_id(self):
        current_user = self.gl.user
        user_id = current_user.id
        return(user_id)

    def get_issue_info(self, issue_url):
        """Fetch JSON representation of issue by issue URL
        directly from API
        """
        o = urlparse(issue_url)
        full_path = o.path.split('/')
        project_path = []
        for p in full_path:
            if p == '-':
                break
            if p:
                project_path.append(p)

        project_name = '/'.join(project_path)
        project_path_id = url_quote_plus(project_name)
        issue_num = full_path[-1]

        uri_path = "/projects/{}/issues/{}".format(project_path_id, issue_num)
        logging.debug("Fetching issue info: {}".format(uri_path))

        project_issue = self.gl.http_get(path=uri_path)

        return project_issue

    def get_issue_raw(self, issue):
        """Fetch JSON representation of the Issue
        directly from API"""
        project_path_id = issue.project_id
        issue_num = issue.iid

        if self.issue_raw_cache is not None:
            if (project_path_id, issue_num) in self.issue_raw_cache:
                logging.debug("Using cached ({},{})".format(
                    project_path_id, issue_num))
                return self.issue_raw_cache[project_path_id, issue_num]

        uri_path = "/projects/{}/issues/{}".format(project_path_id, issue_num)
        logging.debug("Fetching: {}".format(uri_path))

        project_issue = self.gl.http_get(path=uri_path)

        if self.issue_raw_cache is not None:
            self.issue_raw_cache[(project_path_id, issue_num)] = project_issue
            logging.debug("Caching ({},{})".format(project_path_id, issue_num))

        return project_issue

    def api_request(self, **kwargs):
        return self.gl.http_request(**kwargs)

    def get_issue_by_url(self, issue_url):
        """Get Issue by URL"""
        if self.issue_url_cache is not None:
            if issue_url in self.issue_url_cache:
                logging.debug("Using cached {}".format(issue_url))
                return self.issue_url_cache[issue_url]

        issue_info = self.get_issue_info(issue_url)
        project_id = issue_info['project_id']
        # issue_id=issue_info['id']
        issue_iid = issue_info['iid']
        project = self.gl.projects.get(project_id)
        issue = project.issues.get(issue_iid)
        if self.issue_url_cache is not None:
            self.issue_url_cache[issue_url] = issue
            logging.debug("Caching {}".format(issue_url))
        return issue

    def get_issue_by_ref(self, ref):
        """Get Issue by provided ref"""
        if self.issue_ref_cache is not None:
            if ref in self.issue_ref_cache:
                logging.debug("Using cached {}".format(ref))
                return self.issue_ref_cache[ref]
        project_id, issue_iid = ref.split("#")
        issue = self.get_issue_by_id(project_id, issue_iid)
        if self.issue_ref_cache is not None:
            self.issue_ref_cache[ref] = issue
            logging.debug("Caching {}".format(ref))
        return issue

    def get_issue_by_id(self, project_id, issue_iid):
        """Get Issue by Issue IID and Project ID"""
        if self.issue_id_cache is not None:
            if (project_id, issue_iid) in self.issue_id_cache:
                logging.debug("Using cached ({},{})".format(
                    project_id, issue_iid))
                return self.issue_id_cache[project_id, issue_iid]

        # project = self.gl.projects.get(project_id)
        project = self.get_project_by_id(project_id)
        issue = project.issues.get(issue_iid)
        if self.issue_id_cache is not None:
            self.issue_id_cache[project_id, issue_iid] = issue
            logging.debug("Caching ({},{})".format(project_id, issue_iid))
        return issue

    def get_epic_info(self, epic_url):
        """Fetch JSON representation of issue by issue URL
        directly from API
        """
        o = urlparse(epic_url)
        full_path = o.path.split('/')
        group_path = []
        for p in full_path:
            if p == '-':
                break
            if p:
                group_path.append(p)

        group_name = '/'.join(group_path)
        # group_path_id = url_quote_plus(group_name)
        epic_num = full_path[-1]

        uri_path = "/{}/epics/{}".format(group_name, epic_num)
        logging.debug("Fetching Epic info: {}".format(uri_path))

        epic = self.gl.http_get(path=uri_path)

        return epic

    def get_epic_list(self, group_id):
        """Get Epic list"""

        group = self.gl.groups.get(group_id)
        epic_list = group.epics.list(all=True)

        return epic_list

    def get_epic_descendants(self, epic):
        """Get Epic list"""

        # Doesn't work despite what's in
        # ee/app/finders/epics_finder.rb and ee/lib/api/epic_links.rb
        # group = self.gl.groups.get(epic.group_id)
        # epic_list = group.epics.list(all=True, parent_id=epic.id)

        # would be nice:
        # return epic.epics.list(all=True)

        # https://gitlab.com/api/v4/groups/9970/epics/6783/epics
        uri_path = "/groups/{}/epics/{}/epics".format(epic.group_id, epic.iid)
        logging.debug("Fetching Epic descendants: {}".format(uri_path))

        epic_list_json = self.gl.http_get(path=uri_path)
        epic_list = []
        epic_ref_list = []
        for ej in epic_list_json:
            epic_list.append(self.get_epic_by_id(ej['group_id'], ej['iid']))
            epic_ref_list.append(ej['references']['full'])

        logging.debug("Epic {} Descendants: {}".format(epic.references['full'], str(epic_ref_list) ))
        return epic_list

    def get_epic_by_url(self, epic_url):
        """Get Epic by URL"""
        if self.epic_url_cache is not None:
            if epic_url in self.epic_url_cache:
                logging.debug("Using cached {}".format(epic_url))
                return self.epic_url_cache[epic_url]

        epic_info = self.get_epic_info(epic_url)
        group_id = epic_info['group_id']
        epic_iid = epic_info['iid']
        group = self.gl.groups.get(group_id)
        epic = group.epics.get(epic_iid)
        if self.epic_url_cache is not None:
            self.epic_url_cache[epic_url] = epic
            logging.debug("Caching {}".format(epic_url))
        return epic

    def get_epic_by_ref(self, ref):
        """Get Epic by provided ref"""
        if self.epic_ref_cache is not None:
            if ref in self.epic_ref_cache:
                logging.debug("Using cached {}".format(ref))
                return self.epic_ref_cache[ref]
        group_id, epic_iid = ref.split("&")
        epic = self.get_epic_by_id(group_id, epic_iid)
        if self.issue_ref_cache is not None:
            self.issue_ref_cache[ref] = epic
            logging.debug("Caching {}".format(ref))
        return epic

    def get_epic_by_id(self, group_id, epic_iid):
        """Get Issue by Issue IID and Project ID"""
        if self.epic_id_cache is not None:
            if (group_id, epic_iid) in self.epic_id_cache:
                logging.debug("Using cached ({},{})".format(
                    group_id, epic_iid))
                return self.issue_id_cache[group_id, epic_iid]

        group = self.gl.groups.get(group_id)
        # project = self.get_project_by_id(project_id)
        epic = group.epics.get(epic_iid)
        if self.epic_id_cache is not None:
            self.epic_id_cache[group_id, epic_iid] = epic
            logging.debug("Caching ({},{})".format(group_id, epic_iid))
        return epic

    def close_issue(self, issue):
        issue.state_event = 'close'
        if not self.dry_run:
            issue.save()
        else:
            logging.info("Would close {}".format(issue.references['full']))

    def issue_new_comment(self, issue, comment):
        """Add new discussion with a comment"""
        if not self.dry_run:
            new_note = issue.discussions.create({'body': comment})
        else:
            new_note = None
            logging.info("Would comment on {}: {}".format(
                issue.references['full'], comment))
        # TODO invalidate cache
        return new_note

    def label_entry(self, entry, add_labels=[], delete_labels=[]):
        for dl in delete_labels:
            entry.labels.remove(dl)
        for al in add_labels:
            entry.labels.append(al)
        if not self.dry_run:
            # TODO update cache
            entry.save()
        else:
            logging.info("Would label ", entry, " with: ", entry.labels)

    def label_issue(self, issue, add_labels=[], delete_labels=[]):
        self.label_entry(issue, add_labels, delete_labels)

    def label_mr(self, mr, add_labels=[], delete_labels=[]):
        self.label_entry(mr, add_labels, delete_labels)

    def get_user_issues(self):
        user_id = self.get_user_id()
        my_issues = self.gl.issues.list(all=True, assignee_id=user_id)
        return my_issues

    def get_mr_info(self, mr_url):
        """Fetch JSON representation of MR by MR URL"""
        o = urlparse(mr_url)
        full_path = o.path.split('/')
        project_path = []
        for p in full_path:
            if p == '-':
                break
            if p:
                project_path.append(p)

        project_name = '/'.join(project_path)
        project_path_id = url_quote_plus(project_name)
        mr_num = full_path[-1]

        uri_path = "/projects/{}/merge_requests/{}".format(
            project_path_id, mr_num)

        logging.debug("Fetching MR Info: {}".format(uri_path))
        project_mr = self.gl.http_get(path=uri_path)

        return project_mr

    def get_mr_raw(self, mr):
        """Fetch JSON representation of MR"""

        try:
            project_path_id = mr.project_id
            mr_num = mr.iid
        except AttributeError as ae:
            logging.error(str(mr))
            raise ae

        if self.mr_raw_cache is not None:
            if (project_path_id, mr_num) in self.mr_raw_cache:
                logging.debug("Using cached ({},{})".format(
                    project_path_id, mr_num))
                return self.mr_raw_cache[project_path_id, mr_num]

        uri_path = "/projects/{}/merge_requests/{}".format(
            project_path_id, mr_num)

        logging.debug("Fetching MR Raw: {}".format(uri_path))
        project_mr = self.gl.http_get(path=uri_path)

        if self.mr_raw_cache is not None:
            self.mr_raw_cache[(project_path_id, mr_num)] = project_mr
            logging.debug("Caching ({},{})".format(project_path_id, mr_num))

        return project_mr

    def get_mr_by_url(self, mr_url):
        """Get MR by URL"""
        if self.mr_url_cache is not None:
            if mr_url in self.mr_url_cache:
                logging.debug("Using cached {}".format(mr_url))
                return self.mr_url_cache[mr_url]

        mr_info = self.get_mr_info(mr_url)
        project_id = mr_info['project_id']
        mr_iid = mr_info['iid']
        # project = self.gl.projects.get(project_id)
        project = self.get_project_by_id(project_id)
        mr = project.mergerequests.get(mr_iid)
        if self.mr_url_cache is not None:
            self.mr_url_cache[mr_url] = mr
            logging.debug("Caching {}".format(mr_url))
        return mr

    def get_mr_by_id(self, project_id, mr_iid):
        """Get MR by project ID and MR IID"""
        # project = self.gl.projects.get(project_id)
        project = self.get_project_by_id(project_id)
        if self.mr_id_cache is not None:
            if (project_id, mr_iid) in self.mr_id_cache:
                logging.debug("Using cached ({},{})".format(
                    project_id, mr_iid))
                return self.mr_id_cache[project_id, mr_iid]

        mr = project.mergerequests.get(mr_iid)

        if self.mr_id_cache is not None:
            self.mr_id_cache[project_id, mr_iid] = mr
            logging.debug("Caching ({},{})".format(project_id, mr_iid))

        return mr

    def get_mr_by_ref(self, ref):
        """Get MR by ref"""
        if self.mr_ref_cache is not None:
            if ref in self.mr_ref_cache:
                logging.debug("Using cached {}".format(ref))
                return self.mr_ref_cache[ref]
        project_id, mr_iid = ref.split("!")
        mr = self.get_mr_by_id(project_id, mr_iid)
        if self.mr_ref_cache is not None:
            self.mr_ref_cache[ref] = mr
            logging.debug("Caching {}".format(ref))
        return mr

    def watch_projects(self, projects, unwatch=False):
        for p_name in projects:
            p = self.gl.projects.get(p_name)
            ns = p.notificationsettings.get()
            old_level = ns.level
            logging.debug("Current state: {} -> {}".format(p_name, ns.level))
            if unwatch:
                ns.level = gitlab.NOTIFICATION_LEVEL_GLOBAL
            else:
                ns.level = gitlab.NOTIFICATION_LEVEL_WATCH
            if not self.dry_run:
                ns.save()
                print("Changed {}: {} -> {}".format(p_name, old_level, ns.level))
            else:
                logging.info(
                    "Would change {}: {} -> {}".format(p_name, old_level, ns.level))

    def list_label_events(self, resource):
        label_events = resource.resourcelabelevents.list(all=True)
        event_list = []
        for e in label_events:
            new_event = {}
            # TODO Rather crude approach of extracting all interesting \
            # attributes. Perhaps we need to fetch raw JSON
            for attr_name in ['action', 'attributes', 'created_at', 'get_id',
                              'id', 'issue_iid', 'merge_request_iid', 'mr_iid',
                              'label', 'project_id',
                              'resource_id', 'resource_type', 'user']:
                try:
                    attr = getattr(e, attr_name)
                except AttributeError:
                    continue
                # TODO fix PEP8 non-compliance:
                # https://www.flake8rules.com/rules/E721.html
                if type(attr) == type(automator.get_issue_raw):
                    attr_value = attr()
                else:
                    attr_value = attr
                new_event[attr_name] = attr_value
            event_list.append(new_event)
        return event_list

    def get_events(self, options):
        if options:
            gl_events = self.gl.events.list(all=True, **options)
        else:
            gl_events = self.gl.events.list(all=True)
        return gl_events

    def get_simple_events(self, options):
        if options:
            gl_events = self.gl.events.list(all=True, **options)
        else:
            gl_events = self.gl.events.list(all=True)

        events = []

        for event in gl_events:
            project = self.get_project_by_id(event.project_id)
            simple_event = {
                'event_id': event.id,
                'project_id': event.project_id,
                'project': project.path_with_namespace,
                'project_name': project.name,
                'project_url': project.http_url_to_repo,
                'action_name': event.action_name,
                'created_at': event.created_at
                }
            if event.target_type == 'Issue':
                simple_event['issue'] = event.target_iid
            elif event.target_type == 'MergeRequest':
                simple_event['mr'] = event.target_iid
            elif event.target_type in ['Note', 'DiscussionNote', 'DiffNote']:
                if event.note['noteable_type'] == 'Issue':
                    simple_event['issue'] = event.note['noteable_iid']
                elif event.note['noteable_type'] == 'MergeRequest':
                    simple_event['mr'] = event.note['noteable_iid']
            else:
                try:
                    simple_event['ref'] = project.path_with_namespace
                    simple_event['git_ref'] = event.push_data['ref']
                    simple_event['title'] = event.push_data['commit_title']
                except AttributeError:
                    logging.debug("Incomplete simple_event: {}".format(str(simple_event)))
                    pass
            if 'issue' in simple_event:
                issue_iid = simple_event['issue']
                issue = self.get_issue_by_id(event.project_id, issue_iid)

                simple_event['title'] = issue.title
                simple_event['ref'] = issue.references["full"]
                simple_event['url'] = issue.web_url
            if 'mr' in simple_event:
                mr_iid = simple_event['mr']
                mr = self.get_mr_by_id(event.project_id, mr_iid)
                simple_event['title'] = mr.title
                simple_event['ref'] = mr.references['full']
                simple_event['url'] = mr.web_url
            events.append(simple_event)
        return events

    def get_environment_raw(self, environment):

        project_path_id = environment.project['id']
        environment_id = environment.id

        uri_path = "/projects/{}/environments/{}".format(project_path_id, environment_id)
        logging.debug("Fetching: {}".format(uri_path))

        project_environment = self.gl.http_get(path=uri_path)

        return project_environment

    def get_environments(self, project, brief=True, options={}):
        environments = project.environments.list(all=True, **options)
        if brief:
            return environments
        else:
            env_list = []
            for e in environments:
                env_list.append(project.environments.get(e.id))
            return env_list

    def get_simple_environments(self, project, brief=True, options={}):
        environments = self.get_environments(project, brief=True, options=options)
        env_list = []
        for e in environments:
            if brief:
                brief_fields = ['id', 'name', 'slug', 'state', 'created_at', 'updated_at', 'external_url', 'project']
                brief_info = {}
                for bf in brief_fields:
                    brief_info[bf] = getattr(e, bf)
                env_list.append(brief_info)
            else:
                e_info = self.get_environment_raw(e)
                env_list.append(e_info)
                
        return env_list

    def get_pipelines(self, project, options={}):
        pipelines = project.pipelines.list(all=True, **options)
        logging.debug("With options: {} found {} results".format(str(options), len(pipelines)))
        return pipelines

    def run_pipeline(self, project, ref, variables={}, options={}):
        return project.pipelines.create({'ref': ref, 'variables': variables}, **options)
    
    def get_pipeline(self, project, pipeline_id, options={}):
        return project.pipelines.get(pipeline_id, **options)

    def get_jobs(self, pipeline):
        jobs = pipeline.jobs.list(all=True)
        return jobs

    def _graphql_call(self, query):
        uri_path = self.gl.url+"/api/graphql"
        logging.debug("Fetching: {}".format(uri_path))

        logging.debug('Sending data: {}'.format(str(query)))
        result = self.gl.http_post(path=uri_path, post_data=query)

        return result

    def get_repositories(self, project, options={}):
        repositories = project.repositories.list(all=True, **options)
        logging.debug("With options: {} found {} results".format(str(options), len(repositories)))
        return repositories

    def get_container_tags(self, repo, options={}):
        repo_id = repo.id
        query_template = Template("""
            query getTagsFor {
                # https://docs.gitlab.com/ee/api/graphql/reference/#querycontainerrepository
                # https://docs.gitlab.com/ee/api/graphql/reference/#containerrepositoryid
                containerRepository(id: "gid://gitlab/ContainerRepository/${repo_id}"){
                    # https://docs.gitlab.com/ee/api/graphql/reference/#containerrepositorydetails
                    # https://docs.gitlab.com/ee/api/graphql/reference/#containerrepositorydetailstags
                    tags ${after} {
                        # https://docs.gitlab.com/ee/api/graphql/reference/#containerrepositorytagconnection
                        nodes {
                            # https://docs.gitlab.com/ee/api/graphql/reference/#containerrepositorytag
                            name
                            createdAt
                            totalSize
                            digest
                            revision
                            shortRevision
                        }
                        pageInfo {
                            endCursor
                            hasNextPage
                        }
                    }
                }
            }
        """)
        query = query_template.substitute(repo_id=repo_id, after="")
        # logging.debug(query)
        result = self._graphql_call({"query": query})
        tags = result.json()["data"]["containerRepository"]["tags"]["nodes"]
        pageInfo = result.json()["data"]["containerRepository"]["tags"]["pageInfo"]
        logging.debug(pageInfo)
        while pageInfo["hasNextPage"] and (not options.get('one_page', False)):
            after_str = "(after: \"{}\")".format(pageInfo['endCursor'])
            query = query_template.substitute(repo_id=repo_id, after=after_str)
            result = self._graphql_call({"query": query})
            try:
                next_tags = result.json()["data"]["containerRepository"]["tags"]["nodes"]
                pageInfo = result.json()["data"]["containerRepository"]["tags"]["pageInfo"]
                tags.extend(next_tags)
            except Exception as e:
                print(dir(result))
                print(result.json())
                print(result.content)
                raise e
        # print(dir(result))
        # print(result.json())
        return tags
   
    def get_repositories_raw2(self, project, options={}, expand_tags=False):
        query = """
            query {{
                project(fullPath:"{0}") {{
                    repository {{
                        id
                        name
                        path
                        project_id
                        location
                        created_at
                        cleanup_policy_started_at
                        tags {{
                            name
                            path
                            location
                            revision
                            short_revision
                            digest
                            created_at
                        }}
                    }}
                }}
            }}
        """.format(project.path_with_namespace)
        result = self._graphql_call({"query": query})
        print(result.content)
        print(dir(result))
        return result

    def get_repositories_raw(self, project, options={}, expand_tags=False):
        repositories = self.get_repositories(project, options)
        repos_raw = []
        for repo in repositories:
            r = repository2dict(repo)
            if options['tags']:
                tags = []
                r['tags'] = tags
                tag_fields = ['name', 'path', 'location']
                if expand_tags:
                    tag_fields.extend(['revision', 'short_revision', 'digest', 'created_at'])
                for brief_tag in repo.tags.list(all=True):
                    if expand_tags:
                        tag = repo.tags.get(brief_tag.name)
                    else:
                        tag = brief_tag
                    # logging.debug("{} {} {}".format(str(dir(tag)), str(tag), str(extended_tag)))
                    t = {}
                    for tf in tag_fields:
                        t[tf] = getattr(tag, tf)
                    for ta in tag.attributes:
                        t[ta] = tag.attributes[ta]
                    tags.append(t)

            repos_raw.append(r)
        return repos_raw


def load_flow(config_file='triage.yaml'):
    with open(config_file, 'r') as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
    return data


def get_watched(flow):
    return flow['watch']


def walk_epic(automator, epic, epic_action, issue_action,
              epic_parent=None,
              link_types=None, shallow_links=False, visited=None, leaf_state=None):
    if visited is not None and epic.references['full'] in visited:
        return visited
    if visited is not None:
        visited_list = visited
    else:
        visited_list = set()
    visited_list.add(epic.references['full'])
    # if epic_list is None:
    #     all_epics = automator.get_epic_list(epic.group_id)
    # else:
    #     all_epics = epic_list

    sub_epics = automator.get_epic_descendants(epic)
    # sub_epics  = []
    # for e in all_epics:
    #     if e.parent_iid == epic.iid:
    #         sub_epics.append(e)

    walked = []
    for sub_epic in sub_epics:
        logging.debug("Walking Epic: {}".format(sub_epic.references['full']))
        walked = walk_epic(automator=automator,
                           epic=sub_epic,
                           epic_action=epic_action,
                           epic_parent=epic,
                           issue_action=issue_action,
                           link_types=link_types,
                           shallow_links=shallow_links,
                           visited=visited_list,
                           leaf_state=leaf_state)
        logging.debug("Walked {}: {}".format(sub_epic.references['full'], str(walked)))
        visited_list.update(walked)

    for epic_issue in epic.issues.list():
        full_issue = automator.get_issue_by_id(epic_issue.project_id, epic_issue.iid)
        logging.debug("Walking Issue: {}".format(epic_issue.references['full']))
        try:
            walked = walk_issue(automator=automator,
                                issue=full_issue,
                                action=issue_action,
                                link_types=link_types,
                                shallow_links=shallow_links,
                                visited=visited_list,
                                leaf_state=leaf_state)
            visited_list.update(walked)
            logging.debug("Walked {}: {}".format(epic_issue.references['full'], str(walked)))
        except IssueVisisted as iv:
            logging.debug("Double-visited: {}".format(str(iv)))
    epic_action(automator, epic, epic_parent)
    return visited_list


def walk_issue(automator, issue, action, link_types=None, shallow_links=False, visited=None, leaf_state=None):
    """Walks through linked issues and executes action depth-first"""
    if visited is None:
        visited_list = set()
    else:
        visited_list = visited
    if issue.references['full'] in visited_list:
        raise IssueVisisted(issue, visited_list)
    visited_list.add(issue.references['full'])
    if leaf_state is not None and issue.state == leaf_state:
        action(automator, issue)
        return visited_list
    if link_types is not None:
        for link in issue.links.list():
            walked = []
            if (link.link_type in link_types) or ((link_types is None) and shallow_links):
                linked_issue = automator.get_issue_by_id(
                    link.project_id, link.iid)
                if leaf_state is not None and linked_issue.state == leaf_state:
                    # we're at the leaf, move on
                    action(automator, linked_issue)
                    continue
                try:
                    if shallow_links:
                        # We should not process children of children,
                        my_link_types = None
                        # we need shallow links to be false for the next iteration
                        # so that only link_type is matched
                        my_shallow_links = False
                    else:
                        my_link_types = link_types
                        my_shallow_links = shallow_links
                    walked = walk_issue(automator,
                                        linked_issue,
                                        action,
                                        link_types=my_link_types,
                                        shallow_links=my_shallow_links,
                                        visited=visited_list,
                                        leaf_state=leaf_state)
                except IssueVisisted as iv:
                    walked = iv.visited_list
            if walked:
                visited_list.update(walked)
    else:
        for link in issue.links.list():
            walked = []
            linked_issue = automator.get_issue_by_id(
                link.project_id, link.iid)
            if leaf_state is not None and linked_issue.state == leaf_state:
                # we're at the leaf, move on
                action(automator, linked_issue)
                continue
            if link.id in visited_list:
                continue
            if shallow_links:
                action(automator, linked_issue)
                continue
            else:
                try:
                    walked = walk_issue(automator,
                                        linked_issue,
                                        action,
                                        link_types=link_types,
                                        shallow_links=shallow_links,
                                        visited=visited_list,
                                        leaf_state=leaf_state)
                except IssueVisisted as iv:
                    walked = iv.visited_list
            if walked:
                visited_list.update(walked)
    action(automator, issue)
    return visited_list


if __name__ == "__main__":
    def watch_command(automator, args):
        flow = load_flow(args.config)
        automator.watch_projects(get_watched(flow), unwatch=args.reset_watch)

    def list_issues_command(automator, args):
        my_issues = automator.get_user_issues()
        if args.format == 'text':
            for issue in my_issues:
                print("{} {}\n\t{}\n\t{}".format(
                    issue.id, issue.project_id, issue.title, str(issue.labels)))
        elif args.format == 'json':
            issues_list = []
            for issue in my_issues:
                issue_raw = automator.get_issue_raw(issue)
                issues_list.append(issue_raw)
            print(json.dumps(issues_list))

    def _assemble_issue(automator, issue,
                        link_types=None, visited=set(), shallow_links=False):
        """
        link_type filters out dependency tree to avoid cyclical
        graphs
        link_type:
            * 'is_blocked_by'
            * 'blocks'
            * None - skip linked_issues
        """
        visited_list = visited
        issue_raw = automator.get_issue_raw(issue)
        logging.debug("Visiting issue: {} {}".format(
            issue.id, str(visited_list)))
        if issue.id in visited:
            logging.debug("Already visited: {}".format(issue.id))
            raise IssueVisisted()
        visited_list.add(issue.id)

        issue_raw['related_merge_requests'] = issue.related_merge_requests()

        # Label events
        issue_raw['label_events'] = automator.list_label_events(issue)

        # Discussions
        issue_raw['discussions'] = []
        for d in issue.discussions.list(all=True):
            issue_raw['discussions'].append(d.attributes['notes'])

        issue_raw['linked_issues'] = []
        for link in issue.links.list():
            if shallow_links:
                issue_raw['linked_issues'].append(
                    (link.link_type, automator.get_issue_raw(link)))
            elif link_types is not None and link.link_type in link_types:
                # link_raw=automator.get_issue_raw(link)
                link_issue = automator.get_issue_by_id(
                    link.project_id, link.iid)
                try:
                    linked_issue_raw = _assemble_issue(
                        automator, link_issue, link_types, visited_list)
                except IssueVisisted:
                    linked_issue_raw = automator.get_issue_raw(issue)
                    issue_raw['linked_issues'].append(
                        (link.link_type+'_cycle', linked_issue_raw))
                    continue
                issue_raw['linked_issues'].append(
                    (link.link_type, linked_issue_raw))

        return issue_raw

    def get_issue_command(automator, args):
        """
        collect all issue-related information into a single
        JSON object.
        Added components:
          * related_merge_requests
          * discussions
          * label_events
        """

        # issue_url='https://gitlab.com/gitlab-org/charts/components/gitlab-logger/-/issues/4'
        if args.issue_url:
            issue = automator.get_issue_by_url(args.issue_url)
        elif args.issue_ref:
            issue = automator.get_issue_by_ref(args.issue_ref)

        issue_full = _assemble_issue(
            automator, issue, link_types=args.link_types, shallow_links=args.shallow_links)
        print(json.dumps(issue_full, indent=2))

    def get_mr_command(automator, args):
        """
        collect all MR-related information into a single
        JSON object.
        Added components:
          * label_events
        """
        if args.mr_url:
            mr = automator.get_mr_by_url(args.mr_url)
        elif args.mr_ref:
            # gitlab-org/cloud-native/gitlab-operator!198
            mr = automator.get_mr_by_ref(args.mr_ref)
        mr_raw = automator.get_mr_raw(mr)
        mr_raw['label_events'] = automator.list_label_events(mr)

        mr_raw['discussions'] = []
        for d in mr.discussions.list(all=True):
            mr_raw['discussions'].append(d.attributes['notes'])

        closes_issues = mr.closes_issues()
        mr_raw['closes_issues'] = []
        for issue in closes_issues:
            logging.debug(issue)
            mr_raw['closes_issues'].append({'issue': issue.iid, 'title': issue.title, 'url': issue.web_url})

        print(json.dumps(mr_raw, indent=2))

    def clone_labels_command(automator, args):
        if args.from_mr:
            # from_entry_ref=args.from_mr
            from_entry = automator.get_mr_by_ref(args.from_mr)
        elif args.from_issue:
            # from_entry_ref=args.from_issue
            from_entry = automator.get_issue_by_ref(args.from_issue)
        if args.to_mr:
            to_entry_ref = args.to_mr
            to_entry = automator.get_mr_by_ref(args.to_mr)
        elif args.to_issue:
            to_entry_ref = args.to_issue
            to_entry = automator.get_issue_by_ref(args.to_issue)
        from_labels = from_entry.labels
        to_labels = to_entry.labels
        print("{} {} -> {}".format(to_entry_ref, str(to_labels), str(from_labels)))
        to_entry.labels = from_labels
        if not args.dry_run:
            to_entry.save()

    def change_labels_command(automator, args):
        if args.mr_ref:
            entry = automator.get_mr_by_ref(args.mr_ref)
        elif args.issue_ref:
            entry = automator.get_issue_by_ref(args.issue_ref)
        for d in args.delete_labels:
            entry.labels.remove(d)
        for a in args.add_labels:
            entry.labels.append(a)
        if not args.dry_run:
            entry.save()

    def apply_labels_command(automator, args):
        if args.mr_ref:
            entry = automator.get_mr_by_ref(args.mr_ref)
        elif args.issue_ref:
            entry = automator.get_issue_by_ref(args.issue_ref)

        flow = load_flow()
        label_set = flow['labels'][args.label_set]
        if args.reset:
            entry.labels = label_set
        else:
            for ls in label_set:
                entry.labels.append(ls)

        if not args.dry_run:
            entry.save()

    def close_issue_command(automator, args):
        def close_issue_callback(automator, callback_issue):
            if callback_issue.state != "closed":
                automator.issue_new_comment(callback_issue, comment)
                automator.close_issue(callback_issue)

        issues = []
        if args.issue_url:
            for issue_url in args.issue_url:
                issues.append(automator.get_issue_by_url(issue_url))
        elif args.issue_ref:
            for issue_ref in args.issue_ref:
                issues.append(automator.get_issue_by_ref(issue_ref))
        if args.comment == "-":
            comment = sys.stdin.read()
        else:
            comment = args.comment
        # close_issue(issue, comment, link_type=args.link_type)
        for issue in issues:
            if args.link_types:
                walk_issue(automator, issue, close_issue_callback,
                           shallow_links=args.shallow_links,
                           link_types=args.link_types)
            else:
                close_issue_callback(automator, issue)

    def comment_issue_command(automator, args):
        def comment_issue_callback(automator, issue):
            automator.issue_new_comment(issue, comment)

        if args.issue_url:
            issue = automator.get_issue_by_url(args.issue_url)
        elif args.issue_ref:
            issue = automator.get_issue_by_ref(args.issue_ref)
        if args.comment == "-":
            comment = sys.stdin.read()
        else:
            comment = args.comment
        # close_issue(issue, comment, link_type=args.link_type)
        walk_issue(automator, issue, comment_issue_callback,
                   shallow_links=args.shallow_links,
                   link_types=args.link_types)

    def resolve_command(automator, args):
        resolve_dict = {}
        extended = True if args.format == 'extended' else False
        short = True if args.format == 'short' else False
        for uri in args.items:

            item = automator.parse_uri(uri, extended)
            if extended:
                resolve_dict[uri] = item
            elif short:
                resolve_dict[uri] = item.title
        # print(resolve_dict)
        print(json.dumps(resolve_dict))

    def events_command(automator, args):
        options = {}
        if args.before:
            options['before'] = args.before
        if args.after:
            options['after'] = args.after
        if not args.raw:
            raw_events = automator.get_simple_events(options)
            if args.filter_for:
                filtered_events = []
                filter_set = set(args.filter_for)
                for e in raw_events:
                    key_set = set(e.keys())
                    if filter_set.intersection(key_set):
                        filtered_events.append(e)
                raw_events = filtered_events
            events = {}
            if args.aggregate == 'action':
                for event in raw_events:
                    project = event["project"]
                    action = event["action_name"]
                    if project not in events:
                        events[project] = {}
                    if action not in events[project]:
                        events[project][action] = []
                    if 'mr' in event:
                        entry = {'mr': event['mr'], 'title': event['title'], 'created_at': event['created_at']}
                    elif 'issue' in event:
                        entry = {'issue': event['issue'], 'title': event['title'], 'created_at': event['created_at']}
                    elif 'git_ref' in event:
                        entry = {'git_ref': event['git_ref'], 'created_at': event['created_at']}
                    else:
                        logging.error(event)
                    if entry:
                        events[project][action].append(entry)
            elif args.aggregate == 'ref':
                for event in raw_events:
                    project = event["project"]
                    action = event["action_name"]
                    try:
                        ref = event['ref']
                    except KeyError:
                        ref = project.path_with_namespace
                    if project not in events:
                        events[project] = {}
                    if ref not in events[project]:
                        events[project][ref] = {'events': []}
                    if 'mr' in event:
                        entry = {'mr': event['mr'], 'title': event['title']}
                    elif 'issue' in event:
                        entry = {'issue': event['issue'], 'title': event['title']}
                    elif 'git_ref' in event:
                        entry = {'git_ref': event['git_ref']}
                    else:
                        logging.error(event)
                    if entry:
                        events[project][ref].update(entry)
                        events[project][ref]['events'].append(event['created_at'])
            elif args.aggregate == 'ref-brief':
                pre_events = {}
                for event in raw_events:
                    project = event["project"]
                    action = event["action_name"]
                    try:
                        ref = event['ref']
                    except KeyError:
                        ref = project.path_with_namespace
                    if project not in pre_events:
                        pre_events[project] = {}
                    if ref not in pre_events[project]:
                        pre_events[project][ref] = {'events': 0}
                    if 'mr' in event:
                        entry = {'mr': event['mr'], 'title': event['title']}
                    elif 'issue' in event:
                        entry = {'issue': event['issue'], 'title': event['title']}
                    elif 'git_ref' in event:
                        entry = {'git_ref': event['git_ref']}
                    else:
                        logging.error("Cannot understand event: {}".format(str(event)))
                    if 'url' in event and entry:
                        entry['url'] = event['url']
                        logging.debug("{} updated URL".format(str(entry)))
                    else:
                        entry['url'] = event['project_url']
                    entry['project_url'] = event['project_url']
                    if entry:
                        pre_events[project][ref].update(entry)
                        pre_events[project][ref]['events'] += 1
                for project in pre_events:
                    events[project] = []
                    for event_name in pre_events[project]:
                        event = pre_events[project][event_name]
                        ref_or_title = event['git_ref'] if 'git_ref' in event else event['title']
                        events[project].append({'title': ref_or_title,
                                                'ref': event_name,
                                                'url': event['url'],
                                                'project_url': event['project_url'],
                                                'count': event['events']})
            else:
                events = raw_events
            print(json.dumps(events))
        else:
            events = automator.get_events(options)
            if args.filter_for:
                filter_set = set(args.filter_for)
                for event in events:
                    target_type = event.target_type
                    try:
                        target_type = event.note['noteable_type']
                    except AttributeError:
                        pass
                    if target_type:
                        # by now we might have 'Issue' or 'MergeRequest'
                        # we need to convert it back to simpler terms:
                        # 'issue' or 'mr'
                        target_type = TYPE_MAP[target_type]
                    try:
                        if event.push_data:
                            target_type = 'git_ref'
                    except AttributeError:
                        pass
                    if target_type in filter_set:
                        print(event)
            else:
                for event in events:
                    print(event)

    def environments_command(automator, args):
        logging.debug("Getting environment list for project \"{}\"".format(args.project_id))
        project = automator.get_project_by_id(args.project_id[0])
        brief = True if args.format == 'brief' else False
        options = {}
        if args.state:
            options['states'] = args.state
        if args.search:
            options['search'] = args.search
        if args.raw:
            for e_info in automator.get_environments(project, brief=brief, options=options):
                print(e_info)
        else:
            environment_list = automator.get_simple_environments(project, brief=brief, options=options)
            print(json.dumps(environment_list))

    def _pipeline2dict(automator, pipeline):
        pipeline_dict = ['id', 'project_id', 'sha', 'ref', 'status','source', 'created_at', 'updated_at', 'web_url']
        job_dict = ['id', 'status', 'stage', 'name', 'ref', 'tag', 'coverage', 'allow_failure', 'created_at', 
                    'started_at', 'finished_at', 'duration', 'queued_duration', 'user',
                    'commit', 'pipeline', 'web_url', 'artifacts', 'runner', 'artifacts_expire_at', 'tag_list']
        pipeline_json = {}
        for pk in pipeline_dict:
            pipeline_json[pk] = getattr(pipeline, pk)
        job_list = []
        jobs = automator.get_jobs(pipeline)
        for j in jobs:
            job = {}
            for jk in job_dict:
                job[jk] = getattr(j, jk)
            job_list.append(job)
        try:
            vars = pipeline.variables.list(all=True)
        except gitlab.exceptions.GitlabListError as e:
            vars = None
            logging.debug("Can't extract vars {} for {}".format(str(e), str(pipeline)))
        pipeline_json['vars'] = vars
        pipeline_json['jobs'] = job_list
        return pipeline_json
        
    def pipelines_command(automator, args):
        check_options = [
            'scope',
            'status',
            'source',
            'ref',
            'sha',
            'username',
            'updated_before',
            'updated_after'
        ]
        options = {}
        for o in check_options:
            opt = getattr(args, o, None)
            if opt:
                options[o] = opt

        # {'id': 428708269, 'project_id': 18899486, 'sha': '3178b280fd89c571f7d8a5db13ad70ae4822b3bf', 'ref': '235_retool_certification', 'status': 'manual', 'source': 'web', 'created_at': '2021-12-13T21:17:48.117Z', 'updated_at': '2021-12-13T21:31:05.745Z', 'web_url': 'https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/pipelines/428708269'}
        pipeline_dict = ['id', 'project_id', 'sha', 'ref', 'status','source', 'created_at', 'updated_at', 'web_url']

        job_dict = ['id', 'status', 'stage', 'name', 'ref', 'tag', 'coverage', 'allow_failure', 'created_at', 
                    'started_at', 'finished_at', 'duration', 'queued_duration', 'user',
                    'commit', 'pipeline', 'web_url', 'artifacts', 'runner', 'artifacts_expire_at', 'tag_list']
        project = automator.get_project_by_id(args.project_id[0])
        pipelines = automator.get_pipelines(project, options)
        if args.raw:
            for pipeline in pipelines:
                jobs = automator.get_jobs(pipeline)
                print(pipeline)
                for j in jobs:
                    print("-> "+str(j))
                try:
                    vars = pipeline.variables.list(all=True)
                    print(" -::-> "+str(vars))
                except gitlab.exceptions.GitlabListError as e:
                    vars = None
                    logging.debug("Can't extract vars " + str(e))
        else:
            pipeline_list = []
            for p in pipelines:
                pipeline = {}
                for pk in pipeline_dict:
                    pipeline[pk] = getattr(p, pk)
                job_list = []
                jobs = automator.get_jobs(p)
                for j in jobs:
                    job = {}
                    for jk in job_dict:
                        job[jk] = getattr(j, jk)
                    job_list.append(job)
                try:
                    vars = p.variables.list(all=True)
                except gitlab.exceptions.GitlabListError as e:
                    vars = None
                    logging.debug("Can't extract vars {} for {}".format(str(e), str(pipeline)))
                pipeline['vars'] = vars
                pipeline['jobs'] = job_list
                pipeline_list.append(pipeline)
            print(json.dumps(pipeline_list))

    def repositories_command(automator, args):
        project = automator.get_project_by_id(args.project_id[0])
        options = {}
        if args.tags:
            options['tags'] = True
        else:
            options['tags'] = False
        if args.raw:
            repositories = automator.get_repositories(project, options)
            for repo in repositories:
                print(repo)
        else:
            repositories = automator.get_repositories_raw(project, options, args.expand_tags)
            print(json.dumps(repositories))

    def repository_tags_command(automator, args):
        project = automator.get_project_by_id(args.project_id[0])
        options = {}
        repositories = automator.get_repositories(project, options)
        all_tags = []
        options = {'one_page': args.one_page}
        for repo in repositories:
            # repo_dict = repository2dict(repo)
            if repo.name == args.repository:
                tags = automator.get_container_tags(repo, options)
                print(json.dumps(tags))
                all_tags.append(tags)

    def graph_command2(automator, args):
        import graphviz

        edge_colors = {
            'blocks': 'darkgray',
            'is_blocked_by': 'darkred',
            'relates_to': 'darkgreen'
        }
        dot = graphviz.Digraph(comment='Issue graph')
        dot.attr('graph', rankdir=args.orientation)
        nodes = {}
        edges = {}
        # dot = graphviz.Graph(comment='Issue graph')

        def graph_callback(automator, callback_issue):
            issue_reference = callback_issue.references['full']
            if issue is not None and callback_issue.id == issue.id:
                node = {
                    'label': "{}\n({})".format(callback_issue.title, callback_issue.references['full']),
                    'style': 'filled',
                    'color': "black",
                    'fontcolor': "white",
                    'href': callback_issue.web_url,
                    'root': "true"
                }

                node_spec = callback_issue.references['full']
                if node_spec in nodes:
                    logging.warning('Revisiting node {}'.format(node_spec))
                nodes[node_spec] = node
                # dot.node(callback_issue.references['full'],
                #          label="{}\n({})".format(callback_issue.title, callback_issue.references['full']),
                #          style='filled',
                #          color="black",
                #          fontcolor="white",
                #          href=callback_issue.web_url,
                #          root="true")
            else:
                if callback_issue.state == "closed":
                    color = "green3"
                else:
                    color = "gray"
                node = {
                    'label': "{}\n({})".format(callback_issue.title, callback_issue.references['full']),
                    'style': 'filled',
                    'href': callback_issue.web_url,
                    'color': color
                }
                node_spec = callback_issue.references['full']
                if node_spec in nodes:
                    logging.warning('Revisiting node {}'.format(node_spec))
                nodes[node_spec] = node
                # dot.node(callback_issue.references['full'],
                #          label="{}\n({})".format(callback_issue.title, callback_issue.references['full']),
                #          style='filled',
                #          href=callback_issue.web_url,
                #          color=color)
            if epic:
                edge_spec = (epic.references['full'], issue_reference)
                if edge_spec in edges:
                    logging.warning('Revisiting edge {}'.format(str(edge_spec)))
                edges[edge_spec] = {'color': "gray"}
                # dot.edge(epic.references['full'], issue_reference, color="gray")
            if args.leaf_state is not None and callback_issue.state == args.leaf_state:
                # it's a leaf, no edges to draw
                return
            linked_issues = callback_issue.links.list()
            for li in linked_issues:
                if args.link_types is not None and li.link_type not in args.link_types:
                    continue
                linked_issue_reference = li.references['full']

                edge_spec = (issue_reference, linked_issue_reference)
                if edge_spec in edges:
                    logging.warning('Revisiting edge {}'.format(str(edge_spec)))
                edges[edge_spec] = {
                    'label': li.link_type,
                    'fontcolor': edge_colors[li.link_type],
                    'color': edge_colors[li.link_type]
                }
                # dot.edge(issue_reference,
                #          linked_issue_reference,
                #          label=li.link_type,
                #          fontcolor=edge_colors[li.link_type],
                #          color=edge_colors[li.link_type])

        issue = None
        epic = None
        if args.issue_url:
            issue = automator.get_issue_by_url(args.issue_url)
        elif args.issue_ref:
            issue = automator.get_issue_by_ref(args.issue_ref)
        elif args.epic_url:
            epic = automator.get_epic_by_url(args.epic_url)
        elif args.epic_ref:
            epic = automator.get_epic_by_ref(args.epic_ref)

        if issue is not None:
            dot.attr('graph', root=issue.references['full'])
            try:
                walk_issue(automator, issue, graph_callback,
                           shallow_links=args.shallow_links,
                           link_types=args.link_types, leaf_state=args.leaf_state)
            except IssueVisisted:
                logging.warning("Revisiting: {}".format(str(issue)))

        elif epic is not None:
            # epic_list = automator.get_epic_list(epic.group_id)
            dot.attr('graph', root=epic.references['full'])
            # edges = set()
            # nodes = {}

            def print_epic(my_epic):
                node = {
                    'label': "{}\n({})".format(my_epic.title, my_epic.references['full']),
                    'style': 'filled',
                    'color': "black",
                    'fontcolor': "white",
                    'href': my_epic.web_url,
                    'root': "true"
                }
                node_spec = my_epic.references['full']
                if node_spec in nodes:
                    logging.warning("Revisiting: {}".format(node_spec))
                nodes[node_spec] = node
                # dot.node(my_epic.references['full'],
                #          label="{}\n({})".format(my_epic.title, my_epic.references['full']),
                #          style='filled',
                #          color="black",
                #          fontcolor="white",
                #          href=my_epic.web_url,
                #          root="true")

                sub_epics = automator.get_epic_descendants(my_epic)
                # sub_epics = []
                # for e in epic_list:
                #     if e.parent_iid == my_epic.iid:
                #         sub_epics.append(e)

                for sub_epic in sub_epics:
                    edge_spec = (my_epic.references['full'], sub_epic.references['full'])
                    if edge_spec in edges:
                        logging.warning("Revisiting: {}".format(str(edge_spec)))
                    edges[edge_spec] = {'color': 'gray'}
                    # dot.edge(my_epic.references['full'], sub_epic.references['full'], color='gray')
                    print_epic(sub_epic)
                for epic_issue in my_epic.issues.list(all=True):
                    try:
                        real_issue = automator.get_issue_by_id(epic_issue.project_id, epic_issue.iid)
                    except AttributeError as e:
                        logging.debug(str(dir(epic_issue)))
                        raise e
                    edge_spec = (my_epic.references['full'], epic_issue.references['full'])
                    if edge_spec in edges:
                        logging.warning("Revisiting: {}".format(str(edge_spec)))
                    edges[edge_spec] = {'color': 'gray'}
                    # dot.edge(my_epic.references['full'], epic_issue.references['full'], color='gray')
                    try:
                        walk_issue(automator, real_issue, graph_callback,
                                   shallow_links=args.shallow_links,
                                   link_types=args.link_types, leaf_state=args.leaf_state)
                    except IssueVisisted:
                        logging.warning("Revisiting: {}".format(str(real_issue)))

            print_epic(epic)

        for node_name in nodes:
            dot.node(node_name, **nodes[node_name])
        for edge in edges:
            dot.edge(edge[0], edge[1], **edges[edge])

        print(dot.source)

    def graph_command(automator, args):
        import graphviz

        edge_colors = {
            'blocks': 'darkgray',
            'is_blocked_by': 'darkred',
            'relates_to': 'darkgreen'
        }
        dot = graphviz.Digraph(comment='Issue graph')
        dot.attr('graph', rankdir=args.orientation, stylesheet=args.graph_stylesheet)
        dot.attr('node', shape='rect', color="black", margin="0.2")
        nodes = {}
        edges = {}

        def graph_epic_callback(automator, callback_epic, callback_epic_parent):
            if callback_epic_parent:
                callback_parent_ref = callback_epic_parent.references['full']
            else:
                callback_parent_ref = ''
            logging.debug("Drawing Epic {} with parent {}".format(
                            callback_epic.references['full'],
                            callback_parent_ref))
            epic_reference = callback_epic.references['full']
            if callback_epic.state == "closed":
                color = "darkgreen"
                fontcolor = 'white'
            else:
                color = "gray10"
                fontcolor = 'white'
            node = {
                'label': "{}\n({})".format(callback_epic.title, epic_reference),
                'style': 'filled',
                'href': callback_epic.web_url,
                'fillcolor': color,
                'fontcolor': fontcolor,
                'class': 'epic'
            }
            node_spec = epic_reference
            if node_spec in nodes:
                logging.warning('Revisiting node {}'.format(node_spec))
            nodes[node_spec] = node
            if callback_epic_parent is not None:
                edge_spec = (callback_epic_parent.references['full'], callback_epic.references['full'])
                logging.debug("Linking with parent: {}".format(str(edge_spec)))
                if edge_spec in edges:
                    logging.warning('Revisiting edge {}'.format(str(edge_spec)))
                edges[edge_spec] = {'color': "gray"}
            # if args.leaf_state is not None and callback_epic.state == args.leaf_state:
            #     # it's a leaf, no edges to draw
            #     return
            # epic 
            for epic_issue in callback_epic.issues.list(all=True):
                issue_reference = epic_issue.references['full']
                edge_spec = (callback_epic.references['full'], issue_reference)
                logging.debug("Linking Epic with Issue: {}".format(str(edge_spec)))
                if edge_spec in edges:
                    logging.warning('Revisiting edge {}'.format(str(edge_spec)))
                edges[edge_spec] = {'color': "gray"}
            # dot.edge(epic.references['full'], issue_reference, color="gray")

        def graph_issue_callback(automator, callback_issue):
            issue_reference = callback_issue.references['full']
            logging.debug("Drawing Issue {}".format(issue_reference))
            if issue is not None and callback_issue.id == issue.id:
                # we started from the issue
                node = {
                    'label': "{}\n({})".format(callback_issue.title, callback_issue.references['full']),
                    'style': 'filled',
                    # 'fillcolor': "black",
                    'fillcolor': "gray10",
                    'fontcolor': "white",
                    'href': callback_issue.web_url,
                    'root': "true",
                    'class': 'issue'
                }

                node_spec = callback_issue.references['full']
                if node_spec in nodes:
                    logging.warning('Revisiting node {}'.format(node_spec))
                nodes[node_spec] = node
            else:
                if callback_issue.state == "closed":
                    # color = "green3"
                    color = "darkolivegreen1"
                    extra_class = " closed"
                else:
                    color = "lightgray"
                    extra_class = ''
                node = {
                    'label': "{}\n({})".format(callback_issue.title, callback_issue.references['full']),
                    'style': 'filled',
                    'href': callback_issue.web_url,
                    'fillcolor': color,
                    'class': 'issue'+extra_class
                }
                node_spec = callback_issue.references['full']
                if node_spec in nodes:
                    logging.warning('Revisiting node {}'.format(node_spec))
                nodes[node_spec] = node
            if args.leaf_state is not None and callback_issue.state == args.leaf_state:
                # it's a leaf, no edges to draw
                return
            linked_issues = callback_issue.links.list()
            for li in linked_issues:
                if args.link_types is not None and li.link_type not in args.link_types:
                    continue
                linked_issue_reference = li.references['full']

                edge_spec = (issue_reference, linked_issue_reference)
                if edge_spec in edges:
                    logging.warning('Revisiting edge {}'.format(str(edge_spec)))
                edges[edge_spec] = {
                    'label': li.link_type,
                    'fontcolor': edge_colors[li.link_type],
                    'color': edge_colors[li.link_type],
                    'class': li.link_type
                }

        issue = None
        epic = None
        if args.issue_url:
            issue = automator.get_issue_by_url(args.issue_url)
        elif args.issue_ref:
            issue = automator.get_issue_by_ref(args.issue_ref)
        elif args.epic_url:
            epic = automator.get_epic_by_url(args.epic_url)
        elif args.epic_ref:
            epic = automator.get_epic_by_ref(args.epic_ref)

        if issue is not None:
            dot.attr('graph', root=issue.references['full'])
            try:
                walk_issue(automator, issue, graph_issue_callback,
                           shallow_links=args.shallow_links,
                           link_types=args.link_types, leaf_state=args.leaf_state)
            except IssueVisisted:
                logging.warning("Revisiting: {}".format(str(issue)))

        elif epic is not None:
            # epic_list = automator.get_epic_list(epic.group_id)
            walk_epic(automator=automator,
                      epic=epic,
                      epic_action=graph_epic_callback,
                      issue_action=graph_issue_callback,
                      link_types=args.link_types,
                      shallow_links=args.shallow_links,
                      leaf_state=args.leaf_state)

        for node_name in nodes:
            logging.debug("Adding node {}".format(node_name))
            dot.node(node_name, **nodes[node_name])
        for edge in edges:
            logging.debug("Adding edge {}".format(str(edge)))
            dot.edge(edge[0], edge[1], **edges[edge])

        print(dot.source)

    def api_command(automator, args):
        params = {}
        params['verb'] = args.method
        params['path'] = args.path
        if args.query_data:
            params['query_data'] = json.loads(args.query_data)
        if args.post_data and not args.raw:
            params['post_data'] = json.loads(args.post_data)
        elif args.post_data:
            params['post_data'] = args.post_data
        params['raw'] = args.raw
        try:
            result = automator.api_request(**params)
            if args.method in ['post', 'get']:
                print(json.dumps(result.json()))
            elif args.method == 'delete':
                result_struct = {
                    'code': result.status_code,
                    'reason': result.reason,
                    'text': result.text
                }
                print(json.dumps(result_struct))
        except gitlab.exceptions.GitlabHttpError as e:
            print(e)

    def pipeline_start_command(automator, args):
        project = automator.get_project_by_id(args.project_id)
        variables_dict = {}
        if args.variables_json:
            variables_str = json.loads(args.variables_json)
            variables_dict.update(variables_str)
        if args.variables_file:
            with open(args.variables_file, 'r') as var_file:
                variables_file = json.load(var_file)
                variables_dict.update(variables_file)
        pipeline = automator.run_pipeline(project, args.ref, variables=variables_dict)
        run_res = _pipeline2dict(automator, pipeline)
        print(json.dumps(run_res))

    def pipeline_get_command(automator, args):
        logging.debug("Arguments received for pipeline get: {}".format(str(args)))
        project = automator.get_project_by_id(args.project_id[0])
        pipeline = automator.get_pipeline(project, args.pipeline_id)
        run_res = _pipeline2dict(automator, pipeline)
        print(json.dumps(run_res))

    def pipeline_retry_command(automator, args):
        pass

    def pipeline_jobs_command(automator, args):
        pass

    def pipeline_command(automator, args):
        pipeline_routines = {
            'start': pipeline_start_command,
            'get': pipeline_get_command,
            'retry': pipeline_retry_command,
            'jobs': pipeline_jobs_command
        }
        pipeline_routines[args.pipeline_command](automator, args)

    parser = argparse.ArgumentParser()
    parser.add_argument('--gitlab-url')
    parser.add_argument('--gitlab-instance', default='DEFAULT', 
                        help="Instance section to use from gitlab configuration file (~/.gitlab.conf)")
    parser.add_argument('--gitlab-private-token')
    parser.add_argument('--gitlab-config',
                        help="GitLab configuration file (default is to attempt to load it from either: .gitlab.conf, ~/.gitlab.conf, /etc/gitlab.conf")
    parser.add_argument('--debug', default="ERROR",
                        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'])
    parser.add_argument('--config', default='triage.yaml')
    parser.add_argument('--dry-run', action='store_true', default=False)
    cache_group = parser.add_mutually_exclusive_group()
    cache_group.add_argument('--use-cache', dest="use_cache", action='store_true', default=True)
    cache_group.add_argument('--no-cache', dest="use_cache", action='store_false')

    subparsers = parser.add_subparsers(help="subcommand help", dest='command')

    # Watch
    watch_cmd = subparsers.add_parser('watch', help="Toggle \"watch\" for items listed in --config")
    watch_cmd.add_argument('--reset-watch', action='store_true', default=False, help="Reset \"watch\" for items from --config to their global state")

    # List-Issues
    list_issues_cmd = subparsers.add_parser('list-issues',
                                            help="Provide condensed information about issues assigned to user")
    list_issues_cmd.add_argument('--format',
                                 help='Return list of user assigned issues in specified format',
                                 choices=['text', 'json'],
                                 default='json')

    # Get-Issue
    get_issue_cmd = subparsers.add_parser('get-issue', help="Get issue JSON object. Includes all requisites: MRs, linked issues, discussion items")
    get_issue_cmd.add_argument('--link-types',
                               dest='link_types',
                               action='append',
                               help='If link-type is defined recursively fetch linked issues',
                               default=None
                               )
    get_issue_cmd.add_argument('--shallow-links',
                               help='only fetch direct links, no recursion. Can be used without "--link-type"',
                               action='store_true',
                               default=False
                               )
    issue_group = get_issue_cmd.add_mutually_exclusive_group()
    issue_group.add_argument('--issue-url')
    issue_group.add_argument('--issue-ref')

    # Get-MR
    get_mr_cmd = subparsers.add_parser('get-mr', help="Get MR JSON object. Includes all requisites: linked issues, label events, pipeline, etc.")
    mr_group = get_mr_cmd.add_mutually_exclusive_group()
    mr_group.add_argument('--mr-url')
    mr_group.add_argument('--mr-ref')

    # Clone labels
    clone_labels_cmd = subparsers.add_parser('clone-labels', help="Clone labels from one object (Issue or MR) to another (Issue or MR)")
    clone_from_group = clone_labels_cmd.add_mutually_exclusive_group()
    clone_from_group.add_argument('--from-mr')
    clone_from_group.add_argument('--from-issue')
    clone_to_group = clone_labels_cmd.add_mutually_exclusive_group()
    clone_to_group.add_argument('--to-mr')
    clone_to_group.add_argument('--to-issue')

    # Change labels
    change_labels_cmd = subparsers.add_parser('change-labels', help="Change object's labels (add or delete)")
    change_group = change_labels_cmd.add_mutually_exclusive_group(
        required=True)
    change_group.add_argument('--mr-ref')
    change_group.add_argument('--issue-ref')
    change_labels_cmd.add_argument('--add-label',
                                   dest='add_labels',
                                   default=[],
                                   action='append')
    change_labels_cmd.add_argument('--delete-label',
                                   dest='delete_labels',
                                   default=[],
                                   action='append')
    # Change labels
    apply_labels_cmd = subparsers.add_parser('apply-labels', help="(Re)set object labels to the list of labels defined in --config")
    apply_group = apply_labels_cmd.add_mutually_exclusive_group(required=True)
    apply_group.add_argument('--mr-ref')
    apply_group.add_argument('--issue-ref')
    apply_labels_cmd.add_argument('--label-set', default='default')
    apply_labels_cmd.add_argument(
        '--reset', action='store_true', default=False)

    # Close Issue
    close_issue_cmd = subparsers.add_parser('close-issue', help="Close Issue")
    close_issue_group = close_issue_cmd.add_mutually_exclusive_group(
        required=True)
    close_issue_group.add_argument('--issue-ref', nargs="+", action='append')
    close_issue_group.add_argument('--issue-url', nargs="+", action='append')
    close_issue_cmd.add_argument('--comment', default='-')
    close_issue_cmd.add_argument('--link-type',
                                 dest='link_types',
                                 action='append',
                                 help='If link-type is defined recursively close linked issues depth-first',
                                 default=None
                                 )
    close_issue_cmd.add_argument('--shallow-links',
                                 help='only close direct links, no recursion. Can be used without "--link-type"',
                                 action='store_true',
                                 default=False
                                 )
    # Comment on Issue
    comment_issue_cmd = subparsers.add_parser('comment-issue', help="Add a comment to Issue")
    comment_issue_group = comment_issue_cmd.add_mutually_exclusive_group(
        required=True)
    comment_issue_group.add_argument('--issue-ref')
    comment_issue_group.add_argument('--issue-url')
    comment_issue_cmd.add_argument('--comment', default='-')
    comment_issue_cmd.add_argument('--link-type',
                                   dest='link_types',
                                   action='append',
                                   help='If link-type is defined recursively close linked issues depth-first',
                                   default=None
                                   )
    comment_issue_cmd.add_argument('--shallow-links',
                                   help='only close direct links, no recursion. Can be used without "--link-type"',
                                   action='store_true',
                                   default=False
                                   )
    # Resolve ref to Title
    resolve_cmd = subparsers.add_parser('resolve', help="Resolve ref(s) to a record")
    resolve_cmd.add_argument('--format',
                             choices=['short', 'extended'],
                             default='short',
                             help="Short format only includes Title. Extended format includes entire object in JSON format"
                             )
    resolve_cmd.add_argument('items', nargs='*')

    # Events
    events_cmd = subparsers.add_parser('events',
                                       help="Get user events. Does not include Epic events")
    events_cmd.add_argument('--before', default=None)
    events_cmd.add_argument('--after', default=None)
    events_cmd.add_argument('--aggregate', choices=['action', 'ref', 'ref-brief', 'none'], default='none')
    events_cmd.add_argument('--raw', action="store_true", default=False)
    # events_cmd.add_argument('--raw', action="store_true", default=False)
    events_filter_group = events_cmd.add_argument_group(title='optional filters',
                                                        description="Retrieve only specific types of events. If none specified - retrieve all events.")
    events_filter_group.add_argument('--issues', dest='filter_for', action='append_const', const='issue')
    events_filter_group.add_argument('--mrs', dest='filter_for', action='append_const', const='mr')
    events_filter_group.add_argument('--commits', dest='filter_for', action='append_const', const='git_ref')

    # Environments
    environments_cmd = subparsers.add_parser('environments', help="List project environments")
    environments_cmd.add_argument('project_id', nargs=1)
    environments_cmd.add_argument('--raw', action='store_true', default=False)
    environments_cmd.add_argument('--format', choices=['brief', 'full'], default='brief')
    environments_cmd.add_argument('--state', choices=['available', 'stopped'])
    environments_cmd.add_argument('--search')

    # Pipelines
    pipelines_cmd = subparsers.add_parser('pipelines', help="List project pipelines")
    pipelines_cmd.add_argument('project_id', nargs=1)
    pipelines_cmd.add_argument('--raw', action='store_true', default=False)

    pipelines_cmd.add_argument('--scope', choices=['running', 'pending', 'finished', 'branches', 'tags'])
    pipelines_cmd.add_argument('--status', choices=['created', 'waiting_for_resource', 'preparing', 'pending', 'running', 'success', 'failed', 'canceled', 'skipped', 'manual', 'scheduled'])
    pipelines_cmd.add_argument('--source', choices=['push', 'web', 'trigger', 'schedule', 'api', 'external', 'pipeline', 'chat', 'webide', 'merge_request_event', 'external_pull_request_event', 'parent_pipeline', 'ondemand_dast_scan', 'ondemand_dast_validation'])
    pipelines_cmd.add_argument('--git-ref', dest='ref')
    pipelines_cmd.add_argument('--git-sha', dest='sha')
    pipelines_cmd.add_argument('--username')
    pipelines_cmd.add_argument('--before', dest='updated_before', help="ISO 8601 format")
    pipelines_cmd.add_argument('--after', dest='updated_after', help="ISO 8601 format")

    # Repositories
    repositories_cmd = subparsers.add_parser('repositories', help="List project repositories")
    repositories_cmd.add_argument('project_id', nargs=1)
    repositories_cmd.add_argument('--raw', action='store_true', default=False, help="Use \"raw\" Python objects serialization vs default JSON")
    repositories_cmd.add_argument('--tags', action='store_true', default=False)
    repositories_cmd.add_argument('--expand-tags', action='store_true', default=False)

    # Repository Tags
    repository_tags_cmd = subparsers.add_parser('repository-tags', help="List project's container repositories tags")
    repository_tags_cmd.add_argument('project_id', nargs=1, help="Container registry's project path")
    repository_tags_cmd.add_argument('repository', nargs='?', default="", help="Container registry's repository path within a project. \"\" Refers to \"Root Image\" repository.")
    repository_tags_cmd.add_argument('--one-page', action='store_true', default=False, help="Disable pagination - retrieve only first page of results")

    # Issues Graph
    graph_cmd = subparsers.add_parser('graph', help='Generate Issue relationship graph in Graphviz Dot format')
    graph_group = graph_cmd.add_mutually_exclusive_group(
                        required=True)
    graph_group.add_argument('--issue-ref')
    graph_group.add_argument('--issue-url')
    graph_group.add_argument('--epic-ref')
    graph_group.add_argument('--epic-url')
    graph_cmd.add_argument('--orientation', default="LR", choices=["LR", "TB"])
    graph_cmd.add_argument('--graph-stylesheet', default='graph-styles.css')
    graph_cmd.add_argument('--link-type',
                           dest='link_types',
                           action='append',
                           help='If link-type is defined recursively visit linked issues depth-first',
                           default=None
                           )
    graph_cmd.add_argument('--shallow-links',
                           help='only close direct links, no recursion. Can be used without "--link-type"',
                           action='store_true',
                           default=False
                           )
    graph_cmd.add_argument('--leaf-state', default=None, choices=['all', 'closed', 'opened'])

    api_cmd = subparsers.add_parser('api', help='call API directly')
    api_cmd.add_argument('--method', choices=["get", "post", "delete", "put"], default='get')
    api_cmd.add_argument('--query-data', default=None)
    api_cmd.add_argument('--post-data', default=None)
    api_cmd.add_argument('--files', default=None)
    api_cmd.add_argument('--raw', action='store_true', default=False)
    api_cmd.add_argument('path')

    pipeline_cmd = subparsers.add_parser('pipeline', help="Operate project pipelines")
    pipeline_cmd.add_argument('project_id', nargs=1)
    pipeline_subparser = pipeline_cmd.add_subparsers(help="Pipeline commands", dest="pipeline_command")
    pipeline_start_cmd = pipeline_subparser.add_parser('start', help="start pipeline")
    pipeline_start_cmd.add_argument('--variables-json')
    pipeline_start_cmd.add_argument('--variables-file')
    pipeline_start_cmd.add_argument('--ref')
    pipeline_retry_cmd = pipeline_subparser.add_parser('retry', help="retry pipeline")
    pipeline_get_cmd = pipeline_subparser.add_parser('get',   help="get information about pipeline")
    pipeline_get_cmd.add_argument('--pipeline-id')
    pipeline_jobs_cmd = pipeline_subparser.add_parser('jobs',   help="get pipeline jobs")

    args = parser.parse_args()
    loglevel = getattr(logging, args.debug)
    logging.basicConfig(level=loglevel)
    cmd_map = {
        'list-issues': list_issues_command,
        'get-issue':   get_issue_command,
        'get-mr':      get_mr_command,
        'watch':       watch_command,
        'clone-labels': clone_labels_command,
        'change-labels': change_labels_command,
        'apply-labels': apply_labels_command,
        'close-issue': close_issue_command,
        'comment-issue': comment_issue_command,
        'resolve': resolve_command,
        'events': events_command,
        'environments': environments_command,
        'pipelines': pipelines_command,
        'repositories': repositories_command,
        'repository-tags': repository_tags_command,
        'graph': graph_command,
        'api': api_command,
        'pipeline': pipeline_command
    }

    config = configparser.ConfigParser()
    config_stack = ['.gitlab.conf', os.path.expanduser(
        '~/.gitlab.conf'), '/etc/gitlab.conf']
    if args.gitlab_config:
        config_stack.insert(0, args.gitlab_config)
    config.read(config_stack)

    env = os.environ
    gl_instance = args.gitlab_instance
    # Default should come from config file
    gl_url = config[gl_instance].get('url')
    if 'GITLAB_URL' in env:
        # only set up URL if it's non-empty
        if env['GITLAB_URL']:
            gl_url = env['GITLAB_URL']
    if args.gitlab_url:
        gl_url = args.gitlab_url

    # private token or personal token authentication
    gl_token = config[gl_instance].get('private_token')
    if 'GITLAB_PRIVATE_TOKEN' in env:
        if env['GITLAB_PRIVATE_TOKEN']:
            gl_token = env['GITLAB_PRIVATE_TOKEN']
    if args.gitlab_private_token:
        gl_token = args.gitlab_private_token

    try:
        automator = Automator(gl_url, gl_token, dry_run=args.dry_run)
    except gitlab.exceptions.GitlabAuthenticationError:
        logging.fatal("Can't authenticate")
        parser.print_help()
        sys.exit(1)
    if args.use_cache:
        automator.cache('issue_url', 'issue_ref', 'issue_id', 'issue_raw',
                        'mr_url', 'mr_ref', 'mr_id', 'mr_raw',
                        'project_id', 'epic_url', 'epic_ref', 'epic_id')

    if args.command is None:
        print("Invalid invocation:\n{}\n".format(" ".join(sys.argv)))
        parser.print_help()
    else:
        cmd_map[args.command](automator, args)
