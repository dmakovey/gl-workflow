#!/bin/sh

CMD=${1}
TRIAGE_ISSUE=${2}

json(){
    ./triage-summary.sh "${TRIAGE_ISSUE}"  | python triage-report-json.py
}

translated_json(){
    MY_JSON=$(./triage-summary.sh "${TRIAGE_ISSUE}"  | python triage-report-json.py )
    RESOLVE_JSON=$(jq -r '.issues_history | to_entries | .[] | .key' <(echo "$MY_JSON") | xargs python gl-flow.py resolve )
    jq -s '. as $top | $top[1].issues_history | to_entries | [.[] | .key | { key: $top[0][.], value:  $top[1].issues_history[.] }] | from_entries' <(echo "$RESOLVE_JSON") <(echo "$MY_JSON")
}

report(){
    ./triage-summary.sh "${TRIAGE_ISSUE}"  | python triage-report-json.py | jq -r '.tags | to_entries | .[] | "\( .key ) \( .value )"'
}

end_week(){
    { 
        # echo '```json'
        report | sed -e 's#^#* #g'
        # echo '```'
    } | tee | ./gl-flow.py close-issue --issue-url "${TRIAGE_ISSUE}"
}

case ${CMD} in
translated_json)
    translated_json
    ;;
json)
    json
    ;;
report)
    report
    ;;
end)
    end_week
    ;;
esac