#!/usr/bin/env python

import sys
import json
import re

issue_list = {}
label_summary = {}
data = json.load(sys.stdin)
# match ~foo and ~"foo bar" labels
gitlab_label_re_str = r'(?:~\w[\w\:]*)|(?:~"[\w\:\ ]+")'
gitlab_label_re = re.compile(gitlab_label_re_str)
# plain text labels "<label> : <text>"
plain_label_re = re.compile(r'^.+?(?=\s*\:)')
# adding or removing label ("+" vs "-")
label_exp_re = re.compile(r'(?:[+-]*)(?:'+gitlab_label_re_str+')')
label_operation_re = re.compile(r'^[+-]+')
uri_re = re.compile(
    r'(?:\w+?)://(?:\w+)(?:\.\w+)*(?:\:\d+)?(?:/[\w\.\!\?\#\-]+)*')
path_re = re.compile(r'(?:\S*\/\S+)|(?:\S+\/\S*)')
unlabeled = "No label"
for d in data:
    # remove URIs from data to simplify label extraction:
    d_skip_uris = uri_re.sub("", d)
    all_labels = gitlab_label_re.findall(d_skip_uris)
    if not all_labels:
        all_labels = plain_label_re.findall(d_skip_uris)
        if not all_labels:
            all_labels = (unlabeled,)
    labels = set(all_labels)
    all_label_expressions = label_exp_re.findall(d_skip_uris)

    paths = set(path_re.findall(d))
    uris = set(path_re.findall(d))
    issue_path_list = []
    issue_path_set = set()
    if paths:
        issue_path_set.update(paths)
        # print("issue paths: ",paths)
    if uris:
        issue_path_set.update(uris)
        # print("issue uris: ", uris)
    issue_path_list = list(issue_path_set)

    for le in all_label_expressions:
        if le[0] == '-':
            label = label_operation_re.sub('', le)
            try:
                labels.remove(label)
            except KeyError as ke:
                print("Can't find label {} in {}".format(label, str(labels)))
                # print("All expressions: ",all_label_expressions)
                # print("Labels: ",labels)
                # print("Le: ", le)
                # print("Label: ", label)
                raise ke

    for issue_path in issue_path_list:
        if issue_path in issue_list:
            # issue_list[issue_path].extend(list(labels))
            if all_label_expressions:
                issue_list[issue_path].extend(all_label_expressions)
            else:
                issue_list[issue_path].extend(labels)
        else:
            # issue_list[issue_path]=list(labels)
            if all_label_expressions:
                issue_list[issue_path] = all_label_expressions
            else:
                issue_list[issue_path] = list(labels)
    for label in labels:
        if label in label_summary:
            label_summary[label] += 1
        else:
            label_summary[label] = 1

report = {
    'issues_history': issue_list,
    'tags': label_summary
}
print(json.dumps(report, indent=2))
