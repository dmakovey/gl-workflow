#!/bin/bash

FROM_DATE=${FROM_DATE:-$(date --date='7 days ago' "+%Y-%m-%d")}
AFTER_DATE=$(date --date="${FROM_DATE}" "+%Y-%m-%d")
TO_DATE=${TO_DATE:-$(date "+%Y-%m-%d")}
BEFORE_DATE=$(date --date="${TO_DATE}" "+%Y-%m-%d")
TIMESTAMP=$(date --date="${TO_DATE}" '+%Y%m%d')
GL_FLOW=${GL_FLOW:-"scripts/gl-flow.py"}

$GL_FLOW --use-cache events --after=${AFTER_DATE} --before=${BEFORE_DATE} --aggregate=ref-brief > ${TIMESTAMP}.json

jq -r '. | to_entries | .[] | ( .value[] |= [ "\(.title) [\(.ref)](\(.url))" ] | .value |= flatten ) | "* \(.key)", "  * \(.value[])"' ${TIMESTAMP}.json 
