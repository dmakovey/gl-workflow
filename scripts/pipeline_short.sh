#!/bin/sh

# Script is designed to produce "short" version of pipeline JSON for easier digestion
# requires `gl-flow.py pipeline ...` output

exec jq '.[] | {'\
'    id: .id, '\
'    ref: .ref, '\
'    web_url: .web_url, '\
'    jobs: [ '\
'        .jobs[] | {'\
'            name: .name, '\
'            status: .status, '\
'            stage: .stage, '\
'            user: .user.username, '\
'            user_name: .user.name, '\
'            user_url: .user.web_url, '\
'            started: .started_at, '\
'            finished: .finished_at, '\
'            duration: .duration, '\
'            in_queue: .queued_duration, '\
'            commit: .commit.short_id, '\
'            runner: .runner.name, '\
'            runner_id: .runner.id, '\
'            web_url: .web_url'\
'        } '\
'    ] '\
'}' $@