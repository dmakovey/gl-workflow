# Requirements

* `python3`
* `python-gitlab`

# Setup

## Credentials
along the same line as setup of [python-gitlab](https://python-gitlab.readthedocs.io/en/stable/) scripts requires `.gitlab.conf` file present in one of: `./.gitlab.conf`, `${HOME}/.gitlab.conf` and `/etc/gitlab.conf`: 

```ini
[DEFAULT]
url=https://gitlab.com
# private_token=ABCDFEDXXXXXXXXXXXXXX
```

location of the config file can be overriden with `--gitlab-config` directive.

For added security private token could be configured as env variable:

```shell
export GITLAB_PRIVATE_TOKEN="ABCDFEDXXXXXXXXXXXXXX"
```

Multiple GitLab instances can be configured:

```ini
[DEFAULT]
url=https://gitlab.com
# private_token=ABCDFEDXXXXXXXXXXXXXX
[dev]
url=https://dev.gitlab.org
# private_token=ABCDFEDXXXXXXXXXXXXXX
```

and selected via `--gitlab-instance` option. When `--gitlab-instance` option is omitted `DEFAULT` instance will be used.

## User settings

By default `triage.yaml` is current place for runtime configs, use `--config ...` to override.

### watch

list of projects to watch/un-watch in `ref` format:

```yaml
watch:
    - namespace1/project1
    - namespace2/project2
```


### labels

section with pre-defined label sets

```yaml
labels:
  default:
    - label1
    - label3
  skinny:
    - label2
  fat:
    - label1
    - label2
    - label3
  none: []
```

# Usage

commands:

|      Command      |                                       Description                                        |
| ----------------- | ---------------------------------------------------------------------------------------- |
| `api`             | call API directly                                                                        |
| `list-issues`     | Provide condensed information about issues assigned to user                              |
| `get-issue`       | Get issue JSON object. Includes all requisites: MRs, linked issues, discussion items     |
| `comment-issue`   | Add a comment to Issue                                                                   |
| `close-issue`     | Close Issue                                                                              |
| `graph`           | Generate Issue relationship graph in Graphviz Dot format                                 |
| `get-mr`          | Get MR JSON object. Includes all requisites: linked issues, label events, pipeline, etc. |
| `clone-labels`    | Clone labels from one object (Issue or MR) to another (Issue or MR)                      |
| `change-labels`   | Change object's labels (add or delete)                                                   |
| `apply-labels`    | (Re)set object labels to the list of labels defined in --config                          |
| `resolve`         | Resolve ref(s) to a record                                                               |
| `environments`    | List project environments                                                                |
| `pipelines`       | List project pipelines                                                                   |
| `pipeline`        | Operate project pipelines                                                                |
| `repositories`    | List project repositories                                                                |
| `repository-tags` | List project's container repositories tags                                               |
| `watch`           | Toggle "watch" for items listed in --config                                              |
| `events`          | Get user events. Does not include Epic events                                            |

## Watch

First `triage.yaml` file needs to be set up with `watch` section filled:

```yaml
watch:
    - namespace1/project1
    - namespace2/project2
```

after which to trigger `watch` notification on those projects run:

```shell
./gl-flow.py watch
```

to reset back to global settings:

```shell
./gl-flow.py watch --reset
```

## List-Issues

**TODO** currently in rought shape simply fetch list of issues assigned to user

## Get-Issue

print JSON representation of the issue

```shell
./gl-flow.py get-issue --issue-url 'https://gitlab.com/dmakovey/gl-workflow/-/issues/1
```

it also assembles references that normally are not presented as part of issue via API: discussion and related MRs

## Get-MR

print JSON representation of MR

```shell
./gl-flow.py get-mr --mr-ref 'gitlab-org/cloud-native/gitlab-operator!198'
```

## Clone-Labels

clone labels from issue/MR to issue/MR

```shell
./gl-flow.py clone-labels --from-issue 'dmakovey/gl-workflow#2' --to-issue 'dmakovey/gl-workflow#3'
```
as a result `to-issue` will *only* have labels from `from-issue`

## Change-Labels

manipulate labels ad-hoc: add or remove

```shell
./gl-flow.py change-labels --issue-ref 'dmakovey/gl-workflow#3' --delete-label 'label1' --add-label 'label2'
```

## Apply-Labels

apply labels from pre-defined sets in `triage.yaml`:

```yaml
labels:
  default:
    - label1
    - label3
  skinny:
    - label2
  fat:
    - label1
    - label2
    - label3
  none: []
```
remove all labels:
```shell
./gl-flow.py apply-labels --issue-ref 'dmakovey/gl-workflow#3' --reset --label-set none
```

add labels from pre-set:
```shell
./gl-flow.py apply-labels --issue-ref 'dmakovey/gl-workflow#3' --label-set skinny
```

apply `default` label set:
```shell
./gl-flow.py apply-labels --issue-ref 'dmakovey/gl-workflow#3'
```

note that one can apply `default` label set both explicitely and implicitely.. 

## Close-Issue

Close a single issue:

```shell
python scripts/gl-flow.py --debug=INFO --dry-run close-issue  \
  --issue-url https://gitlab.com/dmakovey/gl-workflow/-/issues/1 \
  --comment "closing issue"
```

or cascade-close multiple issues (cascade closure works when one specifies `--link-type` or `--shallow-links`). 
Cascade closure example (will close '#1' and all issues it `relates_to`):

```shell
python scripts/gl-flow.py --debug=INFO --dry-run close-issue  \
  --issue-url https://gitlab.com/dmakovey/gl-workflow/-/issues/1 \
  --link-type relates_to --comment "testing cascade"
```

## Comment-Issue

similar to [Close-Issue](#Close-Issue) can add new comment thread to one or more (related) issues:

```shell
python scripts/gl-flow.py --debug=INFO --dry-run comment-issue  \
  --issue-url https://gitlab.com/dmakovey/gl-workflow/-/issues/1 \
  --comment "extra comment for the issue"
```

for multiple issues:

```shell
python scripts/gl-flow.py --debug=INFO --dry-run comment-issue  \
  --issue-url https://gitlab.com/dmakovey/gl-workflow/-/issues/1 \
  --link-type relates_to --comment "testing cascade"
```

# Advanced use

## Container tag sizes graph

```shell
python scripts/gl-flow.py repository-tags  gitlab-org/build/CNG gitaly | tee data/gitaly.json
jq 'sort_by(.createdAt) | reverse ' data/gitaly.json | jq -r '[.[] | .totalSize]|to_entries | .[] | "\(.key) \(.value)"' > data/gitaly.dat
gnuplot
```

```plot
> set format y "%6.0s %c"
> plot 'data/gitaly.dat' with boxes
```

# Using container version

this repo has `glflow-wrapper.sh` script that helps to pass call to `gl-flow.py` within container instantiation with all proper
mounts etc. Scripts from `scripts/` are located under `/usr/local/bin/` within container.

Sample usage:

```shell
./glflow-wrapper.sh /usr/local/bin/gl-flow.py get-issue --issue-url https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/issues/248
```

Convenience aliases:

## wrapper-scripts/gl-flow

```shell
wrapper-scripts/gl-flow get-issue --issue-url https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/issues/248
```

## wrapper-scripts/triage-week

```shell
wrapper-scripts/triage-week report 'https://gitlab.com/gitlab-org/distribution/team-tasks/-/issues/901'
```