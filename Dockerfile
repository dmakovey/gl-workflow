FROM python

COPY requirements.txt /tmp/requirements.txt
RUN apt-get update -y \
    && apt-get install -y jq \
    && pip install -r /tmp/requirements.txt
COPY scripts/* /usr/local/bin/

# ENTRYPOINT [ "/bin/sh", "-c", "#(nop)" ]
CMD ["/bin/sh"]