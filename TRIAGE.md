# Reporting

```shell
./triage-summary.sh 'https://gitlab.com/gitlab-org/distribution/team-tasks/-/issues/901'  | python triage-report.py
```

# Triage week end activities

## print report

```shell
$ ./triage-week.sh report 'https://gitlab.com/gitlab-org/distribution/team-tasks/-/issues/901'
{
  "Closed": 4,
  "For Scheduling": 1,
  "Awaiting feedback": 1,
  "For scheduling": 6,
  "Closed (support issue)": 1,
  "needs investigation": 2,
  "Backlog": 1
}
```

## print extended report

to get extended report (that includes translation of ticket numbers into human-readable form):

```shell
./triage-week.sh translated_json https://gitlab.com/gitlab-org/distribution/team-tasks/-/issues/956 )
{
  "Mailroom failing to build": [
    "~\"For Scheduling\""
  ],
  "Geo: Improve error message when running the promote command twice on a rails node": [
    "~\"For Scheduling\""
  ],
  ...
}
```

## end week

close the issue and populate it with report

```shell
$ ./triage-week.sh end 'https://gitlab.com/gitlab-org/distribution/team-tasks/-/issues/901'
```