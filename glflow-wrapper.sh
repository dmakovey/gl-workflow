#!/bin/sh

CONTAINER_IMAGE=${CONTAINER_IMAGE:-"registry.gitlab.com/dmakovey/gl-workflow:main"}
CONFIG_FILE=${CONFIG_FILE:-"${HOME}/.gitlab.conf"}
GITLAB_PRIVATE_TOKEN=${GITLAB_PRIVATE_TOKEN:-""}
GITLAB_URL=${GITLAB_URL:-""}

export GITLAB_PRIVATE_TOKEN GITLAB_URL

case ${0##*/} in
    gl-flow)
        cmd=/usr/local/bin/gl-flow.py
        ;;
    triage-week)
        cmd=/usr/local/bin/triage-week.sh
        ;;
    *)
        cmd=""
        ;;
esac

echo "Received: $@"
docker run \
    -e GITLAB_URL \
    -e GITLAB_PRIVATE_TOKEN \
    -v ${CONFIG_FILE}:/etc/gitlab.conf \
    -w /usr/local/bin \
    -it ${CONTAINER_IMAGE} ${cmd} "$@"